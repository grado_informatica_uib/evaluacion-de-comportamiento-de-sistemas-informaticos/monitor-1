#------------------------------------------------------------------------------
#                           CPU MONITORING SCRIPT
#   This script monitor the CPU usage during 2 seconds using 'top' command 
#   from ubuntu as a university project 
#   https://gitlab.com/grado_informatica_uib/evaluacion-de-comportamiento-de-sistemas-informaticos/monitor-1.git
#
#   Author:     Héctor Mario Medina Cabanelas.
#   Contact:    hector.medina@gitlab.com
#               hmedcab@gmai.com
#   Date:       12 March 2019.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
#                           EXTRACCIÓN DE DATOS 
#------------------------------------------------------------------------------

#   Realizamos 120 iteraciones (-n120) de 'top', cada una tiene una frecuencia de
#   muestro de 1 segundo (-d1)

#   Lo pasamos por un filtro grep mediante una tubería

#   Establecemos como salida estandar un fichero .txt

#top -d1 -n5 | grep '^%Cpu(s):' | sed 's/[.]/,/g' > cpuUsage.txt;


#------------------------------------------------------------------------------
#                           CÁLCULO DE MEDIAS
#------------------------------------------------------------------------------

userAverageUsage=0,0;                 #   Uso del usuario
sysAverageUsage=0,0;                  #   Uso del sistema
globalAverageUsage=0,0;               #   Uso global

contador=1;                         #   Contador

while [ $contador -le 5 ];
do
    userUsage=$(cat cpuUsage.txt | awk -v awk_cnt=$contador 'FNR==awk_cnt'| awk '{print $2}');
    echo $userUsage;
    userAverageUsage=$[userAverageUsage + userUsage];
    contador=$((contador+1));
done

echo $userAverageUsage;
